# DEPRECATED

Please use https://gitlab.com/nxt/public/gitlab-ci-templates instead.
The usage below is already adjusted.

# GitLab CI template for Cruft

This is a template for running [cruft](https://github.com/cruft/cruft/) in GitLab CI.

It runs `cruft check` against an existing cruft template repository to verify if the boilerplate contents is still up-to-date.
If not, the job fails with a warning.

By default, the job runs only for merge requests.

## Usage

```yaml
# .gitlab-ci.yml

include:
  - component: "$CI_SERVER_FQDN/nxt/public/gitlab-ci-templates/cruft@main"

    # Optional
    inputs:
      stage: lint
```

## Overwrite default job configuration

The default configuration can be overwritten by adding a `cruft-check` section to your `.gitlab-ci.yml` file, and then setting the desired configuration.

E.g. to run in merge requests pipelines and main branch:

```yaml
include:
  - component: "$CI_SERVER_FQDN/nxt/public/gitlab-ci-templates/cruft@main"

cruft-check:
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
```
